import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,

        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  DateTime _selectedDate;

  void _incrementCounter() {
    setState(() {

      _counter++;
    });
  }

  void _presentDatePicker(){
showDatePicker(
    context: context,
    initialDate: DateTime.now(),
    firstDate: DateTime(1980),
    lastDate: DateTime.now()
).then((pickedDate) {
 if( pickedDate == null){
   return;
 }
 setState(() {
   _selectedDate = pickedDate;
 });

});
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: Center(

        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Date',
            ),
            Text(
              _selectedDate == null
                  ? 'No Date chosen '
                  : DateFormat.yMd().format(_selectedDate),
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _presentDatePicker,
        tooltip: 'Increment',
        child: Icon(Icons.date_range),
      ),
    );
  }
}
